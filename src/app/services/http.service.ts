import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, forkJoin} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {log} from 'ng-zorro-antd';

@Injectable()
export class HttpService {
  handleError: any;
  constructor(private http: HttpClient) {
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  url = 'https://petstore.swagger.io/v2/pet';

  public getData(): Observable<any[]> {
    const response1 = this.http.get('https://petstore.swagger.io/v2/pet/findByStatus?status=available');
    const response2 = this.http.get('https://petstore.swagger.io/v2/pet/findByStatus?status=pending');
    const response3 = this.http.get('https://petstore.swagger.io/v2/pet/findByStatus?status=sold');
    return forkJoin([response1, response2, response3]);
  }
}
