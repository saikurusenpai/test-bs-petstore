import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  postData = {
  id: null,
  category: {
    id: 122,
    name: 'Dogs'
  },
  name: '',
  photoUrls: [
    ''
  ],
  tags: [
    {
      id: 333,
      name: 'hairy'
    }
  ],
  status: []
};
  url = 'https://petstore.swagger.io/v2/pet';

  isVisible = false;
  name: string = null;
  id: number = null;
  filteredOptions: string[] = [];
  options = ['available', 'pending', 'sold'];
  constructor(private http: HttpClient) {
    this.filteredOptions = this.options;
  }
    showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  onChange(value: string): void {
    this.filteredOptions = this.options.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  onChangeName(event): void {
    this.name = event;
    this.postData.name = this.name;
  }
  onChangeId(event): void {
    this.id = event;
    this.postData.id = this.id;
  }
  // POST запрос для создания нового питомца:
  pushData() {
    this.http.post(this.url, this.postData).toPromise().then(data => {
      console.log(data);
    });
  }
}

