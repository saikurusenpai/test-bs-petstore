import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Store} from './store';
import {Order} from './order';
import {NzMessageService} from 'ng-zorro-antd';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  store: Store;
  order: Order;
  id: number;
  isVisible = false;
  isVisibleDelete = true;
  idO: number;
  petId: number;
  quantity: number;
  shipDate: string;
  status: string;
  complete: boolean;
  postListOrder = {
    id: 1,
    petId: 1,
    quantity: 1,
    shipDate: '2019-11-19T17:30:29.675Z',
    status: 'placed',
    complete: true,
  };


  constructor(private http: HttpClient,
              private message: NzMessageService) { }

  ngOnInit(): void {
    this.http.get('https://petstore.swagger.io/v2/store/inventory').subscribe((data: Store) => this.store = data);
  }
  onChange(event) {
    this.id = event;
  }
  // GET запрос для получения заказа по ID
  pullOrder() {
    this.http.get(`https://petstore.swagger.io/v2/store/order/${this.id}`).subscribe((data: Order) => this.order = data);
  }
  // POST запрос для добавления заказа
  postOrder() {
    this.http.post('https://petstore.swagger.io/v2/store/order', this.postListOrder).toPromise().then(data => {
      this.message.info('Order has been added');
    });
  }
  // DELETE запрос для удаления заказа по ID
  deleteOrder() {
    this.http.delete(`https://petstore.swagger.io/v2/store/order/${this.id}`).toPromise().then(data => {
      this.message.info('Order has been deleted');
    });
  }
  showDelete(): void {
    this.isVisibleDelete = false;
  }
  showModal(): void {
    this.isVisible = true;
  }
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }
  onChangeId(event) {
    this.idO = event;
    this.postListOrder.id = this.idO;
  }
  onChangePetId(event) {
    this.petId = event;
    this.postListOrder.petId = this.petId;
  }
  onChangeQuantity(event) {
    this.quantity = event;
    this.postListOrder.quantity = this.quantity;
  }
  onChangeShipDate(event) {
    this.shipDate = event;
    this.postListOrder.shipDate = this.shipDate;
  }
  onChangeStatus(event) {
    this.status = event;
    this.postListOrder.status = this.status;
  }
  onChangeComplete(event) {
    this.complete = event;
    this.postListOrder.complete = this.complete;
  }
}
