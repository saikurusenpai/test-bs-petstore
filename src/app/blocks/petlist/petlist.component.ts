import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http.service';
import {HttpClient} from '@angular/common/http';
import {NzMessageService} from 'ng-zorro-antd';


@Component({
  selector: 'app-petl',
  templateUrl: './petlist.component.html',
  styleUrls: ['./petlist.component.scss'],
  providers: [HttpService]
})
export class PetListComponent implements OnInit {
  searchValue = '';
  public responseData1: any;
  public responseData2: any;
  public responseData3: any;
  public petStatus: any;

  constructor(private httpService: HttpService,
              private http: HttpClient,
              private message: NzMessageService) {
  }
  // GET запрос через HttpService использует forkJoin для получения данных с нескольких источников
  ngOnInit() {
    this.httpService.getData().subscribe(responseList => {
      this.responseData1 = responseList[0];
      this.responseData2 = responseList[1];
      this.responseData3 = responseList[2];
    });
  }
  // DELETE запрос для удаления питомца
  deletePet(id: number) {
    this.http.delete(`https://petstore.swagger.io/v2/pet/${id}`).toPromise().then(data => {
      this.message.info('Pet has been deleted');
    });
  }
  // Вывод данных в таблицу в соответствии с выбранным листом
  toggleAvailable() {
    this.petStatus = this.responseData1;
  }

  togglePending() {
    this.petStatus = this.responseData2;
  }

  toggleSold() {
    this.petStatus = this.responseData3;
  }
}
