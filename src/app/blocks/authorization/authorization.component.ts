import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../user';

@Component({
  selector: 'app-auth',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent {
  user: User;
  urlReg = 'https://petstore.swagger.io/v2/user';
  urlLog = 'https://petstore.swagger.io/v2/user/login';
  usernameLogin: string;
  passwordLogin: string;
  id: number;
  isVisibleRegister = false;
  isVisibleLogin = false;
  isVisibleDropdown1 = false;
  isVisibleDropdown2 = true;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  createUser = {
  id: 444,
  username: '',
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  phone: '',
  userStatus: 1
  };
  constructor(private http: HttpClient) {
  }
  // Функции для модальных окон
  showModalRegister(): void {
    this.isVisibleRegister = true;
  }
  showModalLogin(): void {
    this.isVisibleLogin = true;
  }
  switchLog() {
    this.isVisibleDropdown1 = !this.isVisibleDropdown1;
    this.isVisibleDropdown2 = !this.isVisibleDropdown2;
  }
  handleOkRegister(): void {
    console.log('Button ok clicked!');
    this.isVisibleRegister = false;
  }
  handleOkLogin(): void {
    console.log('Button ok clicked!');
    this.isVisibleLogin = false;
  }
  handleCancelRegister(): void {
    console.log('Button cancel clicked!');
    this.isVisibleRegister = false;
  }
  handleCancelLogin(): void {
    console.log('Button cancel clicked!');
    this.isVisibleLogin = false;
  }
  // Функции, которые заносят вводимые значения в createUser при изменении модели:
  onChangeID(event): void {
    this.id = event;
    this.createUser.id = this.id;
  }
  onChangeUserName(event): void {
    this.username = event;
    this.createUser.username = this.username;
  }
  onChangeFirstName(event): void {
    this.firstName = event;
    this.createUser.firstName = this.firstName;
  }
  onChangeLastName(event): void {
    this.lastName = event;
    this.createUser.lastName = this.lastName;
  }
  onChangeEmail(event): void {
    this.email = event;
    this.createUser.email = this.email;
  }
  onChangePassword(event): void {
    this.password = event;
    this.createUser.password = this.password;
  }
  onChangePasswordLogin(event): void {
    this.passwordLogin = event;
  }
  onChangePasswordUserName(event): void {
    this.usernameLogin = event;
  }
  onChangePhone(event): void {
    this.phone = event;
    this.createUser.phone = this.phone;
  }
  logOut() {
    // Get запрос на выход из системы
    this.http.get('https://petstore.swagger.io/v2/user/logout').subscribe((data) => console.log('You have logged off'));
  }

  pushDataRegister() {
    // Post запрос на создание нового пользователя
    this.http.post(this.urlReg, this.createUser).toPromise().then(data => {
      console.log(data);
    });
  }
  pushDataLogin() {
    // Имитация входа в систему
    this.http.get(`${this.urlLog}?username=${this.usernameLogin}&password=${this.passwordLogin}`,
      {
        responseType: 'text'
      })
      .subscribe((data) => console.log(data));
  }
}

