import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pet stock shop';
  isVisiblePets = true;
  isVisibleOrders = true;
  isVisibleShowPet = false;
  isVisibleHidePet = true;
  isVisibleShowOrder = false;
  isVisibleHideOrder = true;

  showPets(): void {
    this.isVisiblePets = !this.isVisiblePets;
  }
  showOrders(): void {
    this.isVisibleOrders = !this.isVisibleOrders;
  }
  showHide() {
    this.isVisibleShowPet = !this.isVisibleShowPet;
    this.isVisibleHidePet = !this.isVisibleHidePet;
  }
  showHide2() {
    this.isVisibleShowOrder = !this.isVisibleShowOrder;
    this.isVisibleHideOrder = !this.isVisibleHideOrder;
  }

}
