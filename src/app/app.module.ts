import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { SearchByNamePipe} from './search-by-name';

import { MenuComponent } from './blocks/menu/menu.component';
import { AppComponent } from './app.component';
import {AuthorizationComponent} from './blocks/authorization/authorization.component';
import { PetListComponent } from './blocks/petlist/petlist.component';
import { OrdersComponent } from './blocks/orders/orders.component';

import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AuthorizationComponent,
    PetListComponent,
    OrdersComponent,
    SearchByNamePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgZorroAntdModule
  ],
  bootstrap: [ AppComponent, MenuComponent, AuthorizationComponent, OrdersComponent ],
  providers   : [
    { provide: NZ_I18N, useValue: en_US }
  ]
})
export class AppModule { }
