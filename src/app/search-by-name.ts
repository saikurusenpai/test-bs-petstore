import { Pipe, PipeTransform } from '@angular/core';

// Кастомный пайп для поиска питомца по имени.

@Pipe({ name: 'searchByName' })
export class SearchByNamePipe implements PipeTransform {
  transform(value: any, args?: any): any {

    if (!value) {return null; }
    if (!args) {return value; }

    args = args.toLowerCase();

    // tslint:disable-next-line:only-arrow-functions
    return value.filter(function(item) {
      return JSON.stringify(item).toLowerCase().includes(args);
    });
  }
}
